tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer cmp_ = 0;
  Integer not_cmp_ = 0;
  Integer if_ = 0;
  Integer while_ = 0;
  Integer do_ = 0;
  Integer for_ = 0;
}
//prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje;separator=\"\n\"> start: <name;separator=\"\n\"> ";
prog    : (e+=zakres | e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d});

zakres : ^(BEGIN {enterScope();} (e+=zakres | e+=expr | d+=decl)* {leaveScope();})-> blok(wyr={$e},dekl={$d})
;

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> dziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {globals.hasSymbol($i1.text)}? -> podstaw(p1={$i1.text},p2={$e2.st})
        | INT {numer++;}                     -> int(i={$INT.text},j={numer.toString()})
        | ID  {globals.hasSymbol($ID.text)}? -> id(n={$ID.text}) 
        | ^(CMP  e1=expr e2=expr) {cmp_++;}          -> cmp(p1={$e1.st}, p2={$e2.st},nr={cmp_.toString()})
        | ^(NOT_CMP e1=expr e2=expr) {not_cmp_++;}     -> not_cmp(p1={$e1.st}, p2={$e2.st},nr={not_cmp_.toString()})
        | ^(DO    e1=expr e2=expr) {do_++;}          -> do(p1={$e1.st}, p2={$e2.st},nr={do_.toString()})
        | ^(WHILE e1=expr e2=expr) {while_++;}          -> while(p1={$e1.st}, p2={$e2.st},nr={while_.toString()})
        | ^(FOR   e1=expr e2=expr) {for_++;}          -> for(p1={$e1.st}, p2={$e2.st},nr={for_.toString()})
        | ^(IF    e1=expr e2=expr e3=expr?) {if_++;} -> if(e1={$e1.st},e2={$e2.st},e3={$e3.st},nr={if_.toString()})
    ;
//   
//parms :
//   ^(PARM (^(t+TYP n+=ID))*) -> parms(t={$t},n={$n},sep={", "})
//   ;
//           | ^(DIV   e1=expr e2=expr) {dziel0($e2.text)}? -> dziel(p1={$e1.st},p2={$e2.st})
   
   
// pierwszy.stg
//program(name,deklaracje) ::= <<
//<deklaracje;separator="\n"> 
//start: 
//<name;separator="\n"> 
//>>
//
//int(i,j) ::= <<
//MOV A,#<i> ; po raz <j>
//>>
 