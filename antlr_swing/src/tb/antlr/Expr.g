grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | blok)+ EOF!;

blok : BEGIN^ (stat | blok)* END!
;

stat
    : expr NL -> expr

//    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
//    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
//    | IF w=expr THEN t=(blok|expr) (ELSE e=(blok|expr))? NL -> ^(IF w t e?)
    | PRINT expr NL -> ^(PRINT expr)
    | if_stat NL -> if_stat
    | for_ NL -> for_
    | while_ NL -> while_
    | do_ NL -> do_

    | NL ->
    ;

if_stat
//    : IF^ expr THEN! (blok|expr) (ELSE! (blok|expr))?
    : IF^ expr THEN! (expr_) (ELSE! (expr_))?
    ;

for_
    : FOR^ expr DO! expr_
    ;

while_
    : WHILE^ expr DO! expr_
    ;

do_
    : DO^ expr_ WHILE! expr
    ;

expr
    : expr_
      ( CMP^ expr_
      | NOT_CMP^ expr_
      )*
    ;

expr_
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

PRINT : 'print';

IF : 'if';

ELSE : 'else';

THEN : 'then';

FOR : 'for';

DO : 'do';

WHILE : 'while';

BEGIN : '{';

END : '}';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

CMP
  : '=='
  ;
  
NOT_CMP
  : '!='
  ;
  
PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
