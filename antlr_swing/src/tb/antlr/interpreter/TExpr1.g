tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (dekl | e=expr {drukuj ($e.text + " = " + $e.out.toString());})* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = dziel($e1.out, $e2.out);}
        | ^(PODST i1=ID   e2=expr) {syms.setSymbol($ID.text, $e2.out); $out = $e2.out;}
        | ID                       {$out = syms.getSymbol($ID.text).intValue();}
        | INT                      {$out = getInt($INT.text);}
        ;

dekl    : ^(VAR ID) {syms.newSymbol($ID.text);};