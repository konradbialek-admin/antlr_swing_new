package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {

    GlobalSymbols syms = new GlobalSymbols();
    
	public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}

    protected Integer dziel(Integer e1, Integer e2) throws ArithmeticException {
    	if(e2 == 0)
    		throw new ArithmeticException("Dzielenie przez 0!");
    	return e1 / e2;
    }

	protected void hasSymbol(String var) {
		if(!syms.hasSymbol(var)) {
			throw new RuntimeException("Zmienna niezadeklarowana.");
		}
	}
}
