// $ANTLR 3.5.1 /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g 2022-03-22 18:46:05

package tb.antlr;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class ExprLexer extends Lexer {
	public static final int EOF=-1;
	public static final int BEGIN=4;
	public static final int DIV=5;
	public static final int ELSE=6;
	public static final int END=7;
	public static final int ID=8;
	public static final int IF=9;
	public static final int INT=10;
	public static final int LP=11;
	public static final int MINUS=12;
	public static final int MUL=13;
	public static final int NL=14;
	public static final int PLUS=15;
	public static final int PODST=16;
	public static final int PRINT=17;
	public static final int RP=18;
	public static final int THEN=19;
	public static final int VAR=20;
	public static final int WS=21;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public ExprLexer() {} 
	public ExprLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public ExprLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g"; }

	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			int _type = VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:59:5: ( 'var' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:59:6: 'var'
			{
			match("var"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VAR"

	// $ANTLR start "PRINT"
	public final void mPRINT() throws RecognitionException {
		try {
			int _type = PRINT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:61:7: ( 'print' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:61:9: 'print'
			{
			match("print"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PRINT"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:63:4: ( 'if' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:63:6: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			int _type = ELSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:64:6: ( 'else' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:64:8: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ELSE"

	// $ANTLR start "THEN"
	public final void mTHEN() throws RecognitionException {
		try {
			int _type = THEN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:65:6: ( 'then' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:65:8: 'then'
			{
			match("then"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "THEN"

	// $ANTLR start "BEGIN"
	public final void mBEGIN() throws RecognitionException {
		try {
			int _type = BEGIN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:67:7: ( '{' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:67:9: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BEGIN"

	// $ANTLR start "END"
	public final void mEND() throws RecognitionException {
		try {
			int _type = END;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:69:5: ( '}' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:69:7: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "END"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:71:4: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:71:6: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:71:30: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:73:5: ( ( '0' .. '9' )+ )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:73:7: ( '0' .. '9' )+
			{
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:73:7: ( '0' .. '9' )+
			int cnt2=0;
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt2 >= 1 ) break loop2;
					EarlyExitException eee = new EarlyExitException(2, input);
					throw eee;
				}
				cnt2++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "NL"
	public final void mNL() throws RecognitionException {
		try {
			int _type = NL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:75:4: ( ( '\\r' )? '\\n' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:75:6: ( '\\r' )? '\\n'
			{
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:75:6: ( '\\r' )?
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0=='\r') ) {
				alt3=1;
			}
			switch (alt3) {
				case 1 :
					// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:75:6: '\\r'
					{
					match('\r'); 
					}
					break;

			}

			match('\n'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NL"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:77:4: ( ( ' ' | '\\t' )+ )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:77:6: ( ' ' | '\\t' )+
			{
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:77:6: ( ' ' | '\\t' )+
			int cnt4=0;
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( (LA4_0=='\t'||LA4_0==' ') ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt4 >= 1 ) break loop4;
					EarlyExitException eee = new EarlyExitException(4, input);
					throw eee;
				}
				cnt4++;
			}

			_channel = HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "LP"
	public final void mLP() throws RecognitionException {
		try {
			int _type = LP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:81:2: ( '(' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:81:4: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LP"

	// $ANTLR start "RP"
	public final void mRP() throws RecognitionException {
		try {
			int _type = RP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:85:2: ( ')' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:85:4: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RP"

	// $ANTLR start "PODST"
	public final void mPODST() throws RecognitionException {
		try {
			int _type = PODST;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:89:2: ( '=' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:89:4: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PODST"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:93:2: ( '+' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:93:4: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:97:2: ( '-' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:97:4: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "MUL"
	public final void mMUL() throws RecognitionException {
		try {
			int _type = MUL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:101:2: ( '*' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:101:4: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MUL"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:105:2: ( '/' )
			// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:105:4: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	@Override
	public void mTokens() throws RecognitionException {
		// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:8: ( VAR | PRINT | IF | ELSE | THEN | BEGIN | END | ID | INT | NL | WS | LP | RP | PODST | PLUS | MINUS | MUL | DIV )
		int alt5=18;
		switch ( input.LA(1) ) {
		case 'v':
			{
			int LA5_1 = input.LA(2);
			if ( (LA5_1=='a') ) {
				int LA5_19 = input.LA(3);
				if ( (LA5_19=='r') ) {
					int LA5_24 = input.LA(4);
					if ( ((LA5_24 >= '0' && LA5_24 <= '9')||(LA5_24 >= 'A' && LA5_24 <= 'Z')||LA5_24=='_'||(LA5_24 >= 'a' && LA5_24 <= 'z')) ) {
						alt5=8;
					}

					else {
						alt5=1;
					}

				}

				else {
					alt5=8;
				}

			}

			else {
				alt5=8;
			}

			}
			break;
		case 'p':
			{
			int LA5_2 = input.LA(2);
			if ( (LA5_2=='r') ) {
				int LA5_20 = input.LA(3);
				if ( (LA5_20=='i') ) {
					int LA5_25 = input.LA(4);
					if ( (LA5_25=='n') ) {
						int LA5_30 = input.LA(5);
						if ( (LA5_30=='t') ) {
							int LA5_33 = input.LA(6);
							if ( ((LA5_33 >= '0' && LA5_33 <= '9')||(LA5_33 >= 'A' && LA5_33 <= 'Z')||LA5_33=='_'||(LA5_33 >= 'a' && LA5_33 <= 'z')) ) {
								alt5=8;
							}

							else {
								alt5=2;
							}

						}

						else {
							alt5=8;
						}

					}

					else {
						alt5=8;
					}

				}

				else {
					alt5=8;
				}

			}

			else {
				alt5=8;
			}

			}
			break;
		case 'i':
			{
			int LA5_3 = input.LA(2);
			if ( (LA5_3=='f') ) {
				int LA5_21 = input.LA(3);
				if ( ((LA5_21 >= '0' && LA5_21 <= '9')||(LA5_21 >= 'A' && LA5_21 <= 'Z')||LA5_21=='_'||(LA5_21 >= 'a' && LA5_21 <= 'z')) ) {
					alt5=8;
				}

				else {
					alt5=3;
				}

			}

			else {
				alt5=8;
			}

			}
			break;
		case 'e':
			{
			int LA5_4 = input.LA(2);
			if ( (LA5_4=='l') ) {
				int LA5_22 = input.LA(3);
				if ( (LA5_22=='s') ) {
					int LA5_27 = input.LA(4);
					if ( (LA5_27=='e') ) {
						int LA5_31 = input.LA(5);
						if ( ((LA5_31 >= '0' && LA5_31 <= '9')||(LA5_31 >= 'A' && LA5_31 <= 'Z')||LA5_31=='_'||(LA5_31 >= 'a' && LA5_31 <= 'z')) ) {
							alt5=8;
						}

						else {
							alt5=4;
						}

					}

					else {
						alt5=8;
					}

				}

				else {
					alt5=8;
				}

			}

			else {
				alt5=8;
			}

			}
			break;
		case 't':
			{
			int LA5_5 = input.LA(2);
			if ( (LA5_5=='h') ) {
				int LA5_23 = input.LA(3);
				if ( (LA5_23=='e') ) {
					int LA5_28 = input.LA(4);
					if ( (LA5_28=='n') ) {
						int LA5_32 = input.LA(5);
						if ( ((LA5_32 >= '0' && LA5_32 <= '9')||(LA5_32 >= 'A' && LA5_32 <= 'Z')||LA5_32=='_'||(LA5_32 >= 'a' && LA5_32 <= 'z')) ) {
							alt5=8;
						}

						else {
							alt5=5;
						}

					}

					else {
						alt5=8;
					}

				}

				else {
					alt5=8;
				}

			}

			else {
				alt5=8;
			}

			}
			break;
		case '{':
			{
			alt5=6;
			}
			break;
		case '}':
			{
			alt5=7;
			}
			break;
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'I':
		case 'J':
		case 'K':
		case 'L':
		case 'M':
		case 'N':
		case 'O':
		case 'P':
		case 'Q':
		case 'R':
		case 'S':
		case 'T':
		case 'U':
		case 'V':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z':
		case '_':
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'f':
		case 'g':
		case 'h':
		case 'j':
		case 'k':
		case 'l':
		case 'm':
		case 'n':
		case 'o':
		case 'q':
		case 'r':
		case 's':
		case 'u':
		case 'w':
		case 'x':
		case 'y':
		case 'z':
			{
			alt5=8;
			}
			break;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			{
			alt5=9;
			}
			break;
		case '\n':
		case '\r':
			{
			alt5=10;
			}
			break;
		case '\t':
		case ' ':
			{
			alt5=11;
			}
			break;
		case '(':
			{
			alt5=12;
			}
			break;
		case ')':
			{
			alt5=13;
			}
			break;
		case '=':
			{
			alt5=14;
			}
			break;
		case '+':
			{
			alt5=15;
			}
			break;
		case '-':
			{
			alt5=16;
			}
			break;
		case '*':
			{
			alt5=17;
			}
			break;
		case '/':
			{
			alt5=18;
			}
			break;
		default:
			NoViableAltException nvae =
				new NoViableAltException("", 5, 0, input);
			throw nvae;
		}
		switch (alt5) {
			case 1 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:10: VAR
				{
				mVAR(); 

				}
				break;
			case 2 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:14: PRINT
				{
				mPRINT(); 

				}
				break;
			case 3 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:20: IF
				{
				mIF(); 

				}
				break;
			case 4 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:23: ELSE
				{
				mELSE(); 

				}
				break;
			case 5 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:28: THEN
				{
				mTHEN(); 

				}
				break;
			case 6 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:33: BEGIN
				{
				mBEGIN(); 

				}
				break;
			case 7 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:39: END
				{
				mEND(); 

				}
				break;
			case 8 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:43: ID
				{
				mID(); 

				}
				break;
			case 9 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:46: INT
				{
				mINT(); 

				}
				break;
			case 10 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:50: NL
				{
				mNL(); 

				}
				break;
			case 11 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:53: WS
				{
				mWS(); 

				}
				break;
			case 12 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:56: LP
				{
				mLP(); 

				}
				break;
			case 13 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:59: RP
				{
				mRP(); 

				}
				break;
			case 14 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:62: PODST
				{
				mPODST(); 

				}
				break;
			case 15 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:68: PLUS
				{
				mPLUS(); 

				}
				break;
			case 16 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:73: MINUS
				{
				mMINUS(); 

				}
				break;
			case 17 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:79: MUL
				{
				mMUL(); 

				}
				break;
			case 18 :
				// /home/student/git/antlr_swing_new_/antlr_swing/src/tb/antlr/Expr.g:1:83: DIV
				{
				mDIV(); 

				}
				break;

		}
	}



}
